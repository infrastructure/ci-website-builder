from pathlib import Path

from .modules import cache
from .process import dunecontrol, run_process, remove_dir
from .util import abort, parse_frontmatter, Timings
from . import log

def build_sphinx_doc(prefix,docsets):

    root = Path.cwd().resolve()
    default_output = Path('doc') / 'buildsystem' / 'html'

    t = Timings()

    log.notice('Building Sphinx documentation, output to {}',root / prefix)

    if docsets:
        log.info('Only building docsets: {}',docsets)
        docsets = set(docsets)

    for f in (root / 'content' / 'sphinx').glob('**/*.md'):

        with t.time(f.name):

            if docsets is not None and str(f.relative_to(root)) not in docsets:
                log.info('Skipping Sphinx docset {}',f.relative_to(root))
                continue
            else:
                log.notice('Building Sphinx docset {}',f.relative_to(root))

            frontmatter = parse_frontmatter(f)
            module = frontmatter['module']
            path = Path(frontmatter.get('path',default_output))
            ref = frontmatter.get('branch','master')
            workdir = root / 'tmp' / 'sphinx' / f.stem
            workdir.mkdir(parents=True,exist_ok=True)

            builddir = workdir / 'build'
            remove_dir(builddir)

            modules = cache.module(module).closure(ref)

            for module in modules:
                module.update_worktree(workdir,ref)

            optsfile = workdir / 'sphinx.opts'
            with optsfile.open('w') as opts:
                if 'cmakeflags' in frontmatter:
                    opts.write('CMAKE_FLAGS="{}"\n'.format(frontmatter['cmakeflags']))

            dunecontrol('--opts={}'.format(optsfile),'configure',cwd=workdir,module=module)
            dunecontrol('make','doc',cwd=workdir,only=module)

            origin = workdir / 'build' / module.name / path
            target = root / prefix / 'sphinx' / f.relative_to(root).with_suffix('')

            target.mkdir(parents=True,exist_ok=True)
            remove_dir(target)
            origin.rename(target)

            if docsets is not None:
                docsets.remove(str(f.relative_to(root)))

    t.report(log.notice)

    if docsets is not None and len(docsets) > 0:
        log.critical('Could not find docsets {}',docsets)
        abort()
