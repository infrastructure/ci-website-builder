import shutil
import re
import collections
from pathlib import Path

from .util import parse_frontmatter, ensure_dir, abort, Timings
from .process import doxygen, run_process, remove_dir
from .modules import cache
from . import log

DocSet = collections.namedtuple('DocSet',['name','url','version','ref','brief','modules','main_module','mainpage','file'])

module_header = """


# ====================================
# Doxylocal from {}
# ====================================

"""


def get_file_docsets(f, frontmatter):

    log.debug('Extracting Doxygen docsets for {}',f)

    if len(frontmatter) == 0:
        return

    # First check whether a variable doxygen_modules is set
    doxygen_modules = frontmatter.get('doxygen_modules')

    # If not, check whether is is a release
    if not doxygen_modules:
        if frontmatter.get('version'):
            doxygen_modules = frontmatter.get('modules')

    main_module = frontmatter.get('module','')

    # Finally, this might be a single module
    if not doxygen_modules:
        try:
            doxygen_modules = [frontmatter['module']]
        except KeyError:
            log.critical('{} specifies a Doxygen URL, but cannot find modules to build documentation for',f)
            abort()

    # Determine the branch where to build this piece of documentation
    url = frontmatter['doxygen_url']

    if not isinstance(url,list):
        log.critical('{}: "doxygen_url" must be a list, is {}',f,url)
        abort()

    for u in url:
        if not u.startswith('/doxygen/'):
            log.critical('{}: All Doxygen URLs must start with "/doxygen/" (found: "{}")',f,u)
            abort()

    def parse_entry(name,default):
        r = frontmatter.get(name)
        if not r:
            r = [default]*len(url)
        else:
            if not isinstance(r,list) or len(r) != len(url):
                log.critical('{}: Invalid specification of "{}" (not a list or wrong number of entries)',f,name)
                abort()
        return r

    # Determine the branch.
    ref = parse_entry('doxygen_branch','master')

    # Determine the name of this piece of documentation
    name = parse_entry('doxygen_name','Name not specified')

    # Determine the displayed version of this piece of documentation
    version = parse_entry('doxygen_version','?.?.?')

    # Determine the brief doc of this piece of documentation
    brief = parse_entry('doxygen_brief','')

    # Let users set a main module if this docset is not tied to a specific module
    if main_module is None:
        main_module = parse_entry('main_module','')

    # Let users set a custom main page
    mainpage = parse_entry('doxygen_mainpage',None)

    for i in range(len(url)):
        yield DocSet(
            name = name[i],
            url = url[i],
            version = version[i],
            ref = ref[i],
            brief = brief[i],
            modules = [ cache.module(mod) for mod in doxygen_modules ],
            main_module = main_module,
            mainpage = mainpage[i],
            file = f
            )


def get_docsets():

    root = Path().cwd()

    for f in (root / 'content').glob('**/*.md'):
        frontmatter = parse_frontmatter(f)
        if 'doxygen_url' in frontmatter:
            yield from get_file_docsets(f,frontmatter)



def build_doc(docset,prefix):

    root = Path().cwd().resolve()

    log.notice('Building Doxygen docset {} ({})',docset.url,docset.file.resolve().relative_to(root))

    basedir = root / 'tmp' / 'doxygen'

    workdir = basedir / Path(docset.url).relative_to('/')
    log.debug('Running in workdir {}',workdir)

    ensure_dir(workdir)

    for module in docset.modules:
        module.update_worktree(workdir,docset.ref)

    docdir = workdir / 'doc'
    ensure_dir(docdir)
    outdir = workdir / 'html'
    ensure_dir(outdir)

    doxyfile_template = root / 'doxygen' / 'Doxyfile'
    mainpage_template = root / 'doxygen' / 'mainpage.txt'
    doxygen_header = root / 'public' / 'header' / 'index.html'
    doxygen_footer = root / 'public' / 'footer' / 'index.html'

    doxyfile = docdir / 'Doxyfile'
    mainpage = docdir / 'mainpage.txt'
    shutil.copyfile(str(doxyfile_template),str(doxyfile))

    if not docset.mainpage:
        shutil.copyfile(str(mainpage_template),str(mainpage))
    else:
        log.info('Using custom mainpage: {}',docset.main_module)
        mainpage_module = next(m for m in docset.modules if m.name == docset.main_module)
        mainpage_src = workdir / mainpage_module.name / 'doc' / 'doxygen' / docset.mainpage
        log.info('Using custom mainpage: {}',mainpage_src)
        shutil.copyfile(str(mainpage_src),str(mainpage))

    with doxyfile.open('a') as df:

        # Include the separate list of macro definitions in dune-common if it exists
        macrofile = workdir / 'dune-common' / 'doc' / 'doxygen' / 'doxygen-macros'
        if macrofile.exists():
            log.debug('Including C preprocessor definitions from {}',macrofile)
            with macrofile.open() as mf:
                macros = mf.read()
                df.write(macros)
        else:
            log.debug('Could not find macro file {}, no preprocessor definitions available',macrofile)


        for module in docset.modules:

            log.debug('Adding Doxyfile section for {}',module.name)

            moduledir = workdir / module.name
            doxylocal = moduledir / 'doc' / 'doxygen' / 'Doxylocal'

            if not doxylocal.exists():
                continue

            df.write(module_header.format(module.name))

            with doxylocal.open() as dlf:
                dl = dlf.read()

            dl = dl.replace('@srcdir@',str(moduledir / 'doc' / 'doxygen'))
            dl = dl.replace('@top_srcdir@',str(moduledir))

            df.write(dl)
            df.write("""

STRIP_FROM_INC_PATH += {}
STRIP_FROM_PATH     += {}

""".format(moduledir,moduledir)
                     )

        df.write("""



# ====================================
# final global settings
# ====================================

HTML_HEADER = {header}
HTML_FOOTER = {footer}
PROJECT_NAME = "{name}"
PROJECT_NUMBER = "{version}"
PROJECT_BRIEF = "{brief}"
HTML_OUTPUT = {outdir}
GENERATE_TAGFILE = "{name}.tag"
""".format(
    header = doxygen_header,
    footer = doxygen_footer,
    name = docset.name,
    version = docset.version,
    brief = docset.brief,
    outdir = outdir)
                 )

    remove_dir(outdir)
    doxygen('Doxyfile',cwd=docdir)

    target = root / prefix / Path(docset.url).relative_to('/')

    target.mkdir(parents=True,exist_ok=True)
    remove_dir(target)
    outdir.rename(target)

    # copy customized CSS files
    shutil.copyfile(str(root / 'static' / 'css' / 'doxygen.css'),str(target / 'doxygen.css'))
    shutil.copyfile(str(root / 'static' / 'css' / 'dunedoxygen.css'),str(target / 'dunedoxygen.css'))

    # copy tag files
    shutil.copyfile(str(workdir / "doc" / f"{docset.name}.tag"), str(target / f"{docset.name}.tag"))


def build_doxygen_doc(prefix,docsets=None):

    timings = Timings()

    root = Path().cwd().resolve()

    log.notice('Building Doxygen documentation, output to {}',root / prefix)

    if docsets:
        log.info('Only building docsets: {}',docsets)
        docsets = set(docsets)

    for docset in get_docsets():
        with timings.time(docset.url):
            if docsets is not None and docset.url not in docsets:
                log.info('Skipping Doxygen docset {} ({})',docset.url,docset.file.resolve().relative_to(root))
                continue
            build_doc(docset,prefix)
            if docsets is not None:
                docsets.remove(docset.url)

    timings.report(log.notice)

    if docsets is not None and len(docsets) > 0:
        log.critical('Could not find docsets {}',docsets)
        abort()
