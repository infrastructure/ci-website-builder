from .dependencies import ensure_setuptools_requirement

ensure_setuptools_requirement('PyYAML >= 3.10',name=__name__)
ensure_setuptools_requirement('layered-yaml-attrdict-config',name=__name__)

import yaml
import dune.ciwebsitebuilder.lya as lya

def should_use_block(value):
    for c in u"\u000a\u000d\u001c\u001d\u001e\u0085\u2028\u2029":
        if c in value:
            return True
    return False

def my_represent_scalar(self, tag, value, style=None):
    if style is None:
        if should_use_block(value):
             style='|'
        else:
            style = self.default_style

    node = yaml.representer.ScalarNode(tag, value, style=style)
    if self.alias_key is not None:
        self.represented_objects[self.alias_key] = node
    return node

def enable_block_representer():
    yaml.representer.BaseRepresenter.represent_scalar = my_represent_scalar


# make sure ordered dicts retain their ordering and don't get reordered
# alphabetically
_represent_mapping = yaml.representer.BaseRepresenter.represent_mapping
class ItemsHidingWrapper(object):

    def __init__(self,mapping):
        self.mapping = mapping

    def __iter__(self):
        return iter(self.mapping.items())

def my_represent_mapping(self,tag,value,style=None):
    if isinstance(value,(collections.OrderedDict,lya.AttrDict)):
        value = ItemsHidingWrapper(value)
    return _represent_mapping(self,tag,value,style)

def retain_ordered_dict_order():
    yaml.representer.BaseRepresenter.represent_mapping = my_represent_mapping
