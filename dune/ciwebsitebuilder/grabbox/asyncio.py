import asyncio
from functools import singledispatch
import io
import os
import stat


class PseudoAsyncFileReader:

    def __init__(self, file):
        if not file.readable:
            raise ValueError('{} can only wrap readable files'.format(self.__class__.__name__))
        self._file = file
        self._at_eof = False

    def exception(self):
        return None

    def feed_eof(self):
        raise NotImplemented

    def feed_data(self, data):
        raise NotImplemented

    def set_exception(self, exc):
        raise NotImplemented

    def set_transport(self, transport):
        raise NotImplemented

    @asyncio.coroutine
    def read(self, n=-1):
        if False:
            yield None  # stupid dummy statement to turn this into a generator
        buf = self._file.read(n)
        if n > 0 and not buf:
            self._at_eof = True
        return buf

    @asyncio.coroutine
    def readline(self):
        if False:
            yield None  # stupid dummy statement to turn this into a generator
        buf = self._file.readline()
        if not buf:
            self._at_eof = True
        return buf

    @asyncio.coroutine
    def readexactly(self, n):
        if False:
            yield None  # stupid dummy statement to turn this into a generator
        blocks = []
        while n > 0:
            block = self._file.read(n)
            if not block:
                partial = b''.join(blocks)
                raise asyncio.IncompleteReadError(partial, len(partial) + n)
            blocks.append(block)
            n -= len(block)
        return b''.join(blocks)

    def at_eof(self):
        return self._at_eof


class PseudoAsyncFileWriter:

    def __init__(self, file, ignore_close):
        if not file.writable:
            raise ValueError('{} can only wrap writable files'.format(self.__class__.__name__))
        self._file = file
        self._ignore_close = ignore_close

    @property
    def transport(self):
        raise NotImplemented

    def can_write_eof(self):
        return False

    def close(self):
        if not self._ignore_close:
            self._file.close()

    @asyncio.coroutine
    def drain(self):
        if False:
            yield None  # stupid dummy statement to turn this into a generator
        return

    def get_extra_info(self, name, default=None):
        return default

    def write(self, data):
        self._file.write(data)

    def writelines(self, data):
        for d in data:
            self._file.write(d)

    def write_eof(self):
        raise NotImplemented


@singledispatch
def make_async_reader(file):
    raise ValueError('PseudoAsyncFileReader does not support wrapping {}'.format(type(file)))


@make_async_reader.register(io.BytesIO)
def async_reader_bytes_io(file):
    return PseudoAsyncFileReader(file)


@make_async_reader.register(io.FileIO)
@make_async_reader.register(io.BufferedReader)
@make_async_reader.register(io.BufferedRandom)
def async_reader_file_io(file):
    mode = os.stat(file.fileno()).st_mode
    if not stat.S_ISREG(mode):
        raise ValueError('PseudoAsyncFileReader only supports wrapping regular files')
    return PseudoAsyncFileReader(file)


@singledispatch
def make_async_writer(file, ignore_close=True):
    raise ValueError('PseudoAsyncFileWriter does not support wrapping {}'.format(type(file)))


@make_async_writer.register(io.BytesIO)
def async_writer_bytes_io(file, ignore_close=True):
    return PseudoAsyncFileWriter(file, ignore_close)


@make_async_writer.register(io.FileIO)
@make_async_writer.register(io.BufferedWriter)
@make_async_writer.register(io.BufferedRandom)
def async_writer_file_io(file, ignore_close=True):
    mode = os.stat(file.fileno()).st_mode
    if not stat.S_ISREG(mode):
        raise ValueError('PseudoAsyncFileWriter only supports wrapping regular files')
    return PseudoAsyncFileWriter(file, ignore_close)
