from .dependencies import ensure_package

ensure_package('pathlib',globals(),locals(),name=__name__)


from functools import update_wrapper
import sys, os, subprocess
from contextlib import contextmanager
from pathlib import Path
import tempfile

class memoize(object):

    def __init__(self,func):
        update_wrapper(self,func)
        self.func = func

    def __get__(self,obj,cls):
        if obj is None:
            return self
        value = self.func(obj)
        setattr(obj,self.__name__,value)
        return value


def ensure_min_python_version(major,minor=None,micro=None,name=None):
    from sys import version_info
    sys_major, sys_minor, sys_micro = version_info[:3]
    if sys_major < major:
        raise UnsupportedPythonVersion((major,minor,micro),version_info,failure_level=1,name=name)
    if minor is not None and sys_minor < minor:
        raise UnsupportedPythonVersion((major,minor,micro),version_info,failure_level=2,name=name)
    if micro is not None and sys_micro < micro:
        raise UnsupportedPythonVersion((major,minor,micro),version_info,failure_level=3,name=name)

def ensure_package(package,globals,locals,name=None):
    if '.' in package:
        raise ValueError('ensure_package() can only check for top-level packages, but you provided %s' % package)
    try:
        __import__(package,globals,locals)
    except ImportError as e:
        reraise(MissingPackage(package,name),from_=e)

def ensure_setuptools_requirement(requirement,name=None):
    try:
        import pkg_resources
    except ImportError:
        raise MissingPackage('pkg_resources',name=__name__)
    try:
        pkg_resources.require(requirement)
    except Exception as e:
        reraise(MissingRequirement(requirement,error=e,name=name),from_=e)



class SafeTemporaryDirectory(object):

    def __init__(self,key):
        self.key = key

    @memoize
    def path(self):
        p = Path(os.environ[self.key]).resolve()
        if not p.is_dir():
            raise FileNotFoundError
        return p

    @memoize
    def name(self):
        return self.path

    def safe_tmpdir_root(self):
        if Path('/dev/shm').is_dir():
            return '/dev/shm'
        else:
            return None

    @contextmanager
    def ensure(self):
        try:
            self.path
        except:
            with tempfile.TemporaryDirectory(prefix='vault',suffix='.tmp',dir=self.safe_tmpdir_root()) as d:
                print('Tempdir: {}'.format(d))
                env = os.environ.copy()
                env[self.key] = d
                retcode = subprocess.call(sys.argv,stdin=sys.stdin,stdout=sys.stdout,stderr=sys.stderr,env=env)
            sys.exit(retcode)
        else:
            yield

@contextmanager
def create_private_files():
    oldmask = os.umask(0o077)
    try:
        yield
    finally:
        os.umask(oldmask)

def resolve_path(p):
    return Path(os.path.expandvars(os.path.expanduser(p)))
