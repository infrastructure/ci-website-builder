from .dependencies import ensure_setuptools_requirement, ensure_package
import sys
from contextlib import contextmanager

ensure_setuptools_requirement('LogBook >= 0.10', name=__name__)
ensure_package('enum', globals(), locals(), name=__name__)

from enum import IntEnum
import logbook
from logbook.base import _reverse_level_names

LogLevel = IntEnum('LogLevel', sorted(_reverse_level_names.items(), key=lambda e: e[1]))
LogLevel.min_level = LogLevel.DEBUG
LogLevel.max_level = max(LogLevel)


def _clamp_loglevel(cls, level):
    try:
        return cls(level)
    except ValueError:
        new_level = max(min(level, cls.max_level), cls.min_level)
        return new_level

LogLevel.clamp = classmethod(_clamp_loglevel)


class Logger(logbook.Logger):

    def exception(self, *args, **kwargs):
        """Works exactly like :meth:`error` just that the message
        is optional and exception information is recorded.
        """
        try:
            level = kwargs['level']
        except KeyError:
            level = LogLevel.ERROR
        if self.disabled or level < self.level:
            return
        if not args:
            args = ('Uncaught exception occurred',)
        if 'exc_info' not in kwargs:
            exc_info = sys.exc_info()
            assert exc_info[0] is not None, 'no exception occurred'
            kwargs.setdefault('exc_info', sys.exc_info())
        return self.error(*args, **kwargs)


class TaggingLogger(Logger):

    def __init__(self, *args, **kwargs):
        tags = kwargs.pop('tags', ())
        tag = kwargs.pop('tag', None)
        super().__init__(*args, **kwargs)
        self._tags = set(tags)
        if tag is not None:
            self._tags.add(tag)

    @property
    def tags(self):
        return self._tags

    def process_record(self, record):
        super().process_record(record)
        record.tags = frozenset(self._tags)


@contextmanager
def catch_exceptions(
        logger,
        level=LogLevel.CRITICAL,
        message='Fatal error: {record.exception_shortname}: {record.exception_message}',
        reraise=False,
        ignore_exceptions=(KeyboardInterrupt,),
        log_exceptions=(Exception,),
):
    try:
        yield
    except ignore_exceptions:
        raise
    except log_exceptions as e:
        exc_info = sys.exc_info()
        # build a record by hand so that we can format the message - ugly, I know...
        record = logbook.LogRecord('',level.value,message,[],{},exc_info,None,None,None,0)
        logger.log(level.value, message.format(record=record), exc_info=sys.exc_info())
        if reraise:
            raise
        else:
            try:
                sys.exit(e.exit_code)
            except AttributeError:
                sys.exit(1)
