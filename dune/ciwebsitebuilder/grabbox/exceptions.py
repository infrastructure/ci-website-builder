import sys

PY2 = sys.version_info[0] == 2

if PY2:
    def reraise(e,from_):
        raise e
else:
    # ugly as hell, but the only way to avoid a SyntaxError on Python 2
    exec("""
def reraise(e,from_):
    raise e from from_
""")



class GrabBoxException(Exception):
    pass

class MissingSoftware(GrabBoxException):
    pass

class UnsupportedPythonVersion(MissingSoftware):


    def __init__(self,required,given,failure_level,name):
        required = '.'.join((str(p) for p in required[:failure_level]))
        given = '.'.join((str(p) for p in given[:failure_level]))
        if name:
            super(UnsupportedPythonVersion,self).__init__(
                '%s requires at least Python %s, but you only have Python %s' % (
                    name,
                    required,
                    given
                )
            )
        else:
            super(UnsupportedPythonVersion,self).__init__(
                'Python %s required, but you only have Python %s' % (
                    required,
                    given
                )
            )


class MissingPackage(MissingSoftware):

    def __init__(self,package,name):
        if name:
            super(MissingPackage,self).__init__(
                '%s could not find the required package %s' % (
                    name,
                    package
                )
            )
        else:
            super(MissingPackage,self).__init__(
                'Package %s required, but could not be found' % package
            )


class MissingRequirement(MissingSoftware):

    def __init__(self,requirement,error,name):
        if name:
            super(MissingRequirement,self).__init__(
                '%s could not find the requirement \'%s\' due to: %s: %s' % (
                    name,
                    requirement,
                    error.__class__.__name__,
                    error.args
                )
            )
        else:
            super(MissingRequirement,self).__init__(
                '\'%s\' required, but could not be found due to: %s: %s' % (
                    requirement,
                    error.__class__.__name__,
                    error.args
                )
            )



class ClickException(GrabBoxException):
    pass


class ClickLogbookException(ClickException):

    exit_code = 2
