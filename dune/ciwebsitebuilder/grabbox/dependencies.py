from .exceptions import UnsupportedPythonVersion, MissingPackage, MissingRequirement, reraise


def ensure_min_python_version(major, minor=None, micro=None, name=None):
    from sys import version_info
    sys_major, sys_minor, sys_micro = version_info[:3]
    if sys_major < major:
        raise UnsupportedPythonVersion(
            (major, minor, micro), version_info, failure_level=1, name=name)
    if minor is not None and sys_minor < minor:
        raise UnsupportedPythonVersion(
            (major, minor, micro), version_info, failure_level=2, name=name)
    if micro is not None and sys_micro < micro:
        raise UnsupportedPythonVersion(
            (major, minor, micro), version_info, failure_level=3, name=name)


def ensure_package(package, globals, locals, name=None):
    if '.' in package:
        raise ValueError(
            'ensure_package() can only check for top-level packages, but you provided %s' % package)
    try:
        __import__(package, globals, locals)
    except ImportError as e:
        reraise(MissingPackage(package, name), from_=e)


def ensure_setuptools_requirement(requirement, name=None):
    # return None
    try:
        import pkg_resources
    except ImportError:
        raise MissingPackage('pkg_resources', name=__name__)
    try:
        pkg_resources.require(requirement)
    except Exception as e:
        reraise(MissingRequirement(requirement, error=e, name=name), from_=e)
