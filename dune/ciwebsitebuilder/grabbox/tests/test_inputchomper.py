from grabbox.io import InputChomper
from grabbox.pytest import assert_generator
from collections import namedtuple
import pytest

class ChunkFactory:

    def __init__(self,separator,default_start=b''):
        self.separator = separator
        self.default_start = default_start
        self.default_out = default_start.decode()

    nothing = b''

    def chomper(self,data=None):
        if data is None:
            data = self.default_start
        return InputChomper(separator=self.separator,data=data)

    @property
    def empty(self):
        return self.separator

    def chunk(self,contents):
        if not isinstance(contents,bytes):
            contents = contents.encode()
        return contents + self.separator

@pytest.fixture(params=(
    ChunkFactory(separator=b'\t'),
    ChunkFactory(separator=b'\t',default_start=b'foo'),
    ChunkFactory(separator=b'\n'),
    ChunkFactory(separator=b'\n',default_start=b'foo\t'),
    ChunkFactory(separator=b'\0'),
    ChunkFactory(separator=b'\0',default_start=b'foo\tbar\n'),
    ))
def factory(request):
    return request.param


def test_no_default_leakage(factory):
    ic = factory.chomper()
    assert_generator(ic.read_chunks())


def test_default_chunk(factory):
    ic = factory.chomper()
    assert_generator(ic.read_chunks(factory.separator),factory.default_out)
    assert_generator(ic.read_chunks())


def test_append(factory):
    ic = factory.chomper()

    ic.append(b'append' + factory.separator)
    assert_generator(ic.read_chunks(),factory.default_out + 'append')
    assert_generator(ic.read_chunks())


def test_chunking(factory):
    ic = factory.chomper()

    s = factory.separator

    ic.append(s.join((b'foo',b'bar',b'keep')))
    assert_generator(ic.read_chunks(),factory.default_out + 'foo','bar')

    ic.append(b'more')
    assert_generator(ic.read_chunks())

    ic.append(s)
    assert_generator(ic.read_chunks(),'keepmore')
    assert_generator(ic.read_chunks())


def test_read(factory):
    ic = factory.chomper()

    assert ic.read(0) == ''
    assert ic.read(0) == ''
    assert ic.read() == factory.default_out
    assert ic.read() == ''

    ic = factory.chomper()
    # get rid of old state
    ic.read()
    ic.append(b'four' * 20)
    for i in range(20):
        assert ic.read(4) == 'four'
    assert ic.read() == ''



@pytest.fixture(params=(
    dict(encoding='utf-8',data=''),
    dict(encoding='utf-8',data='abäöüéó'),
    dict(encoding='latin1',data='abäöüéó'),
    dict(encoding=None,data=b''),
    dict(encoding=None,data=b'ab\xf1\x98'),
    ))
def encoding(request):
    param = request.param
    return dict(
        encoding = param['encoding'],
        input = param['data'].encode(encoding=param['encoding']) if param['encoding'] is not None else param['data'],
        output = param['data'],
    )

def test_encoding(encoding):

    ic = InputChomper(data=encoding['input'],encoding=encoding['encoding'])
    out = ic.read()
    assert isinstance(out,encoding['output'].__class__) # check that InputChomper decodes correctly
    assert out == encoding['output']




@pytest.fixture(params=('utf-8',None))
def peek_encoding(request):
    return request.param

def test_peek(peek_encoding):

    input = 'foo'
    output = input if peek_encoding is not None else input.encode()
    ic = InputChomper(data=input.encode(),encoding=peek_encoding)

    assert ic.peek() == output # peek should return the input, decoded if an encoding was given
    assert ic.peek() == output # peek should be idempotent
    assert ic.read() == output # peek should not modify the input

    output = '' if peek_encoding is not None else b''

    assert ic.peek() == output # now the input buffer should be empty
    assert ic.read() == output # again, we must not change the input buffer
