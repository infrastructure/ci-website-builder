import click

from .grabbox.click.logbook import init_logbook, LogHandler
from .grabbox.logbook import catch_exceptions as _catch_exceptions

from . import log
from .modules import cache

def catch_exceptions():
    return _catch_exceptions(
        logger = log,
        ignore_exceptions = (
            KeyboardInterrupt,
            click.ClickException,
            click.Abort,
            ),
        )

@click.group()
@init_logbook(
    logger = log,
    catch_exceptions = catch_exceptions,
    handler = LogHandler(
        colorize=True,
        formatstring='[{record.level_name:<8}] {record.channel}: {record.message}',
        stacktrace=True
    ),
)
def cli():
    """Builds Doxygen and Sphinx documentation for the Dune website."""
    pass


@cli.command(
    name='update-repos'
)
def update_repos():
    """Finds and updates module repositories."""
    cache.populate(update=True)

@cli.command()
@click.option(
    '-p','--prefix',
    type=click.Path(file_okay=False,dir_okay=True),
    default='public',
    help='Prefix for generated docsets'
)
@click.argument(
    'docsets',
    nargs=-1,
    metavar='[DOCSET_URL...]',
)
def sphinx(prefix,docsets):
    """Builds Sphinx documentation.

    This commands builds the docsets given on the command line, or all
    docsets it can find by crawling the sources if given no arguments.
    """
    cache.populate()
    from .sphinx import build_sphinx_doc
    build_sphinx_doc(prefix,docsets if len(docsets) > 0 else None)

@cli.command()
@click.option(
    '-p','--prefix',
    type=click.Path(file_okay=False,dir_okay=True),
    default='public',
    help='Prefix for generated docsets'
)
@click.argument(
    'docsets',
    nargs=-1,
    metavar='[DOCSET_URL...]',
)
def doxygen(prefix,docsets):
    """Builds Doxygen documentation.

    This commands builds the docsets given on the command line, or all
    docsets it can find by crawling the sources if given no arguments.
    """
    cache.populate()
    from .doxygen import build_doxygen_doc
    build_doxygen_doc(prefix,docsets if len(docsets) > 0 else None)
