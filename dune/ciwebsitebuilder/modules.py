import subprocess
import itertools
from collections import OrderedDict
from pathlib import Path
from tempfile import TemporaryFile
from io import StringIO, IOBase
from pyparsing import *
from .grabbox.namedtuple import DefaultableNamedTuple

from . import log
from .util import parse_frontmatter, ensure_dir, abort
from .process import git


class DuneModuleFile(DefaultableNamedTuple):

    def __init__(self, module=None, version=None, maintainer=None, whitespace_hook=False,
                       depends=[], suggests=[], python_modules=[],
                       author="", python_requires=[], url="",
                       description=""):
        pass


class DuneModuleFileParser(object):
    _debug = False

    singleKeys = ["Module", "Version", "Whitespace-Hook", "URL"]
    multiKeys = ["Depends", "Suggests", "Python-Requires"]
    lenientKeys = ["Maintainer", ]
    verbatimKeys = ["Author", "Description" ]

    def __init__(self):
        self._parser = self.construct_bnf()

    def _bnf_from_key(self, key):
        bnf = Literal('{}:'.format(key)).suppress() + Word(printables).setParseAction(self.setVariable(key.replace('-', '_').lower()))
        return bnf

    def _bnf_from_lenient_key(self, key):
        bnf = Literal('{}:'.format(key)).suppress() + OneOrMore(Word(printables)).setParseAction(self.setVariable(key.replace('-', '_').lower()))
        return bnf

    def _bnf_from_verbatim_key(self, key):
        bnf = Literal('{}:'.format(key)).suppress() + OneOrMore(Word(printables)).setParseAction(self.fullVariable(key.replace('-', '_').lower()))
        return bnf

    def _bnf_versioned_list(self, key):
        requirement = Word(printables, excludeChars="()").setParseAction(self.appendVariable(key.lower().replace('-','_')))
        version = Optional(Literal("(").suppress() + Word(printables + " ", excludeChars=")") + Literal(")").suppress())
        # TODO do not suppress the version here, but use it instead
        entry = requirement + version.suppress()
        requlist = ZeroOrMore(entry)
        bnf = Literal('{}:'.format(key)).suppress() + requlist
        return bnf

    def construct_bnf(self):
        simplekeys = [self._bnf_from_key(k) for k in DuneModuleFileParser.singleKeys]
        multikeys = [self._bnf_versioned_list(k) for k in DuneModuleFileParser.multiKeys]
        lenientkeys = [self._bnf_from_lenient_key(k) for k in DuneModuleFileParser.lenientKeys]
        verbatimkeys = [self._bnf_from_verbatim_key(k) for k in DuneModuleFileParser.verbatimKeys]
        comment = [(Literal('#') + restOfLine).suppress(), Empty()]
        bnflist = simplekeys + multikeys + lenientkeys + verbatimkeys + comment

        line = bnflist[0]
        for b in bnflist[1:]:
            line = line | b
        line = line + LineEnd()

        return line

    def appendVariable(self, var):
        def _parseAction(origString, loc, tokens):
            if var not in self.result:
                self.result[var] = []
            self.result[var].append(tokens[0])
        return _parseAction

    def setVariable(self, var):
        def _parseAction(origString, loc, tokens):
            self.result[var] = tokens[0]
        return _parseAction

    def fullVariable(self, var):
        def _parseAction(origString, loc, tokens):
            self.result[var] = ' '.join(t for t in tokens)
        return _parseAction

    def apply(self, f):
        self.result = {}
        for line in f:
            log.debug('Parsing module file line: {}',line.strip())
            try:
                self._parser.parseString(line)
            except:
                log.critical('Bad Dune module line: {}',line)
                abort()

        no_corepy = lambda x : 'dune-corepy' not in x
        try:
            self.result['depends'] = [ m for m in self.result['depends'] if no_corepy(m) ]
        except KeyError:
            pass
        try:
            self.result['suggests'] = [ m for m in self.result['suggests'] if no_corepy(m) ]
        except KeyError:
            pass
        return DuneModuleFile(**self.result)

dune_module_file_parser = DuneModuleFileParser()

def parse_dune_module_file(data):
    if isinstance(data,str):
        return dune_module_file_parser.apply(StringIO(data))
    elif isinstance(data,BaseIO):
        return dune_module_file_parser.apply(data)
    else:
        with data.open() as f:
            return dune_module_file_parser.apply(f)



class Module:

    def __init__(self,cache,name,url=None):
        log.debug('Registering module {} with repository {}',name,url)
        self.cache = cache
        self.name = name
        self.url = url
        self.closures = {}
        self.repo = cache.base_path / '{}.git'.format(name)
        self.materialized = self.repo.exists()
        if self.materialized:
            if self.cache.update:
                log.notice('Module {} (repository {}) already cloned, updating',name,url)
                git('remote','update',cwd=self.repo)

    def materialize(self):
        if self.materialized:
            return

        if not self.cache.update:
            log.critical('Repository cache is in read-only mode, but cannot find repository for {}',self.name)
            abort()

        log.notice('Cloning {} from {}',self.name,self.url)
        if not self.url:
            log.critical('Cannot clone module {} (unknown git repository), please set the git field in the module file',self.name)
            abort()
        git('clone','--mirror',self.url,str(self.repo),cwd=Path().cwd())
        self.materialized = True

    def module_file(self,ref):
        self.materialize()
        log.debug('Getting module file of {} for ref {}',self.name,ref)
        try:
            return parse_dune_module_file(git('show','{}:dune.module'.format(ref),failure=False,cwd=self.repo).stdout)
        except ValueError:
            print("failed with branch",ref,"in",self.repo,"retrying with master")
            return parse_dune_module_file(git('show','master:dune.module',cwd=self.repo).stdout)


    def update_worktree(self,basedir,ref=None):
        self.materialize()
        ref = ref or 'master'
        log.debug('Updating worktree {} for module {} and ref {}',basedir,self.name,ref)
        w = Path(basedir) / self.name
        if not w.exists():
            log.info('Creating worktree for {}@{} in {}',self.name,ref,w)
            try:
                git('worktree','add','-f',str(w),ref,failure=False,cwd=self.repo)
            except ValueError:
                print("failed with branch",ref,"in",self.repo,"retrying with master")
                git('worktree','add','-f',str(w),"master",cwd=self.repo)
        else:
            if git('remote','get-url','origin',cwd=w).stdout.strip() != self.url:
                raise RepositoryError('Directory exists')
            log.info('Updating worktree for {}@{} in {}',self.name,ref,w)
            try:
                git('checkout','--ignore-other-worktrees',ref,failure=False,cwd=w)
                git('reset','--hard',ref,cwd=w)
            except ValueError:
                print("failed with branch",ref,"in",w,"retrying with master")
                git('checkout','--ignore-other-worktrees','master',cwd=w)
                git('reset','--hard','master',cwd=w)

    def closure(self,ref=None):
        ref = ref or 'master'

        self.materialize()

        try:
            return self.closures[ref]
        except KeyError:

            log.debug('Determiming closure of {} for ref {}',self.name,ref)

            module_file = self.module_file(ref)

            closure = []

            for dep in itertools.chain(module_file.depends,module_file.suggests):
                closure.extend(module.name for module in self.cache.module(dep).closure(ref))

            closure.append(self.name)

            closure = tuple(self.cache.module(name) for name in OrderedDict.fromkeys(closure).keys())

            self.closures[ref] = closure
            return closure



class Cache:

    def __init__(self,default_repos={}):
        self.default_repos = default_repos
        self.modules = {}
        self.root = Path().cwd()
        self.repo_root = self.root / 'repositories'
        ensure_dir(self.repo_root)

    def module(self,name,url=None):

        try:
            module = self.modules[name]
            if url and module.url:
                if not url == module.url:
                    log.critical('Module {} is registered at {}, but you passed {}',name,module.url,url)
                    abort()
            if not module.url:
                module.url = url
        except KeyError:
            url = url or self.default_repos.get(name)
            module = Module(self,name,url)
            self.modules[name] = module
        return module

    @property
    def base_path(self):
        return self.repo_root

    def populate(self,update=False):
        root = Path().cwd()
        log.notice('Setting up module cache, mode: {}','updating' if update else 'readonly')
        self.update = update

        for f in (root / 'content' / 'modules').glob('**/*.md'):
            frontmatter = parse_frontmatter(f)
            name = frontmatter.get('module')
            if not name:
                continue
            repo = frontmatter.get('git')
            self.module(name,repo)

        from .doxygen import get_docsets
        for docset in get_docsets():
            for module in docset.modules:
                module.materialize()

        for f in (root / 'content' / 'sphinx').glob('**/*.md'):
            frontmatter = parse_frontmatter(f)
            module = self.module(frontmatter['module'])
            ref = frontmatter.get('branch')
            module.closure(ref)


_all_repos = {'dune-alugrid': 'https://gitlab.dune-project.org/extensions/dune-alugrid.git',
              'dune-common': 'https://gitlab.dune-project.org/core/dune-common.git',
              'dune-geometry': 'https://gitlab.dune-project.org/core/dune-geometry.git',
              'dune-grid': 'https://gitlab.dune-project.org/core/dune-grid.git',
              'dune-grid-howto': 'https://gitlab.dune-project.org/core/dune-grid-howto.git',
              'dune-functions': 'https://gitlab.dune-project.org/staging/dune-functions.git',
              'dune-istl': 'https://gitlab.dune-project.org/core/dune-istl.git',
              'dune-localfunctions': 'https://gitlab.dune-project.org/core/dune-localfunctions.git',
              'dune-pdelab': 'https://gitlab.dune-project.org/pdelab/dune-pdelab.git',
              'dune-pdelab-howto': 'https://gitlab.dune-project.org/pdelab/dune-pdelab-howto.git',
              'dune-pdelab-systemtesting': 'https://gitlab.dune-project.org/quality/dune-pdelab-systemtesting.git',
              'dune-pdelab-tutorials': 'https://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials.git',
              'dune-python': 'https://gitlab.dune-project.org/quality/dune-python.git',
              'dune-testtools': 'https://gitlab.dune-project.org/quality/dune-testtools.git',
              'dune-typetree': 'https://gitlab.dune-project.org/pdelab/dune-typetree.git',
              'dune-multidomaingrid': 'https://gitlab.dune-project.org/extensions/dune-multidomaingrid.git',
              }


cache = Cache(_all_repos)
