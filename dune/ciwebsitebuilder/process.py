import subprocess, sys
from pathlib import Path
import tempfile
import logbook

from . import log
from .util import abort

def run_process(*args,**kwargs):
    kwargs['universal_newlines'] = True
    kwargs['stdout'] = subprocess.PIPE
    kwargs['stderr'] = subprocess.PIPE
    return subprocess.run(*args,**kwargs)

def git(*args,cwd,failure=True,**kwargs):
    command = ['git']
    command.extend(args)
    cmdline = ' '.join(command)
    log.debug('Running {} in {}',cmdline,cwd)
    r = run_process(command,cwd=cwd,**kwargs)
    if r.returncode != 0:
        if failure:
            log.critical('Error running {} in {}\n{}',' '.join(command),cwd,r.stderr)
            abort()
        else:
            raise ValueError('Error running {} in {}\n{}',' '.join(command),cwd,r.stderr)
    return r

def dunecontrol(*args,cwd,module=None,only=None,builddir=None,**kwargs):
    command = [ './dune-common/bin/dunecontrol' ]
    if module is not None:
        try:
            module = ','.join(m.name for m in module)
        except:
            module = module.name
        command.append('--module={}'.format(module))
    if only is not None:
        try:
            only = ','.join(m.name for m in only)
        except:
            only = only.name
        command.append('--only={}'.format(only))
    cwd = cwd.resolve()
    if builddir is None:
        builddir = 'build'
    builddir = Path(builddir)
    if not builddir.is_absolute():
        builddir = cwd / builddir
    command.append('--builddir={}'.format(builddir))
    command.extend(args)
    cmdline = ' '.join(command)
    log.info('Running \'{}\' in {}',cmdline,cwd)
    kwargs['universal_newlines'] = True
    if log.level <= logbook.INFO:
        if subprocess.run(command,cwd=cwd,**kwargs).returncode != 0:
            log.critical('Error running \'{}\' in {}',cmdline,cwd)
            abort()
    else:
        with tempfile.TemporaryFile('w+',encoding='utf8') as output:
            if subprocess.run(command,stdout=output,stderr=subprocess.STDOUT,cwd=cwd,**kwargs).returncode != 0:
                log.critical('Error running \'{}\' in {}',cmdline,cwd)
                output.seek(0)
                for line in output:
                    sys.stdout.write(str(output))
                abort()


def doxygen(*args,cwd,**kwargs):
    command = ['doxygen']
    command.extend(args)
    cmdline = ' '.join(command)
    log.info('Doxygen: Running {} in {}',cmdline,cwd)
    if log.level <= logbook.DEBUG:
        # Just let the Doxygen output pass through
        if subprocess.run(command,cwd=cwd,**kwargs).returncode != 0:
            log.critical('Error running Doxygen command {} in {}',cmdline,cwd)
            abort()
    else:
        # buffer the output in a temporary file, as it is ludicrously long
        with tempfile.TemporaryFile('w+',encoding='utf8') as output:
            if subprocess.run(command,stdout=output,stderr=subprocess.STDOUT,cwd=cwd,**kwargs).returncode != 0:
                log.critical('Error running Doxygen command {} in {}',cmdline,cwd)
                output.seek(0)
                for line in output:
                    sys.stdout.write(str(line))
                abort()


def remove_dir(path):
    log.debug('Removing directory {}',path)
    if subprocess.run(['rm','-rf',str(path)]).returncode != 0:
        log.critical('Failed to remove directory {}',path)
        abort()
