import re
import io
import click
import time
from contextlib import contextmanager
import toml

from . import log


def parse_frontmatter(f):
    with io.open(f) as data:
        marker = data.readline().strip()
        if marker != '+++':
            log.warn('File {} does not have valid start marker "+++", skipping',f)
            return {}
        headers = []
        for line in data:
            if line.strip() == '+++':
                headers = ''.join(headers)
                return toml.loads(headers)
            headers.append(line)
        log.warn('Unterminated header section in file {}, skipping',f)
        return {}


def ensure_dir(path):
    path.mkdir(parents=True,exist_ok=True)


def abort():
    click.get_current_context().abort()


class Timings:

    def __init__(self):
        self.timings = {}

    @contextmanager
    def time(self,name):
        start = time.time()
        yield
        end = time.time()
        self.timings[name] = end - start

    def report(self,log):
        namelength = max(max(len(name) for name in self.timings.keys()),30)
        bar = '*' * (namelength + 12)
        log(bar)
        log('Run Times')
        log(bar)
        logstr = '{{:<{}}}    {{:0>2}}:{{:0>2}}:{{:0>2}}'.format(namelength)
        for name in sorted(self.timings.keys()):
            elapsed = int(self.timings[name])
            elapsed, s = divmod(elapsed,60)
            h, m = divmod(elapsed,60)
            log(logstr,name,h,m,s)

        log(bar)
