FROM debian:stable

# Install packages and create builder user
COPY install.sh /
RUN bash /install.sh

# Build and install our local tools
# Note: '--break-system-packages' avoids issues with pep668.
#       An alternative using a venv is described here
#       https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
COPY dune setup.py /src/dune/
RUN mv /src/dune/setup.py /src \
  && pip3 install /src --break-system-packages \
  && rm -rf /src

# Run as a regular user
USER builder:builder
