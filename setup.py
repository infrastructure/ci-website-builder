from setuptools import setup, PEP420PackageFinder
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name = 'ciwebsitebuilder',

    version = '1.1.0',

    description  = 'Utility for building parts of the Dune website',

    url = 'https://gitlab.dune-project.org/infrastructure/ci-website-builder',

    author = 'Steffen Müthing',
    author_email = 'steffen.muething@iwr.uni-heidelberg.de',

    license='BSD',

    classifiers=[  # Optional
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',

        'License :: OSI Approved :: BSD License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],

    packages=PEP420PackageFinder.find(),

    setup_requires=[
        'setuptools_scm',
    ],

    install_requires = [
        'arrow',
        'click',
        'ipython',
        'logbook',
        'nbsphinx',
        'pyparsing',
        'sphinxcontrib-bibtex',
        'sphinx_rtd_theme',
        'sphinx>=4',
        'toml',
        'setuptools',
        ],

    entry_points={
        'console_scripts' : [
            'ciwebsitebuilder=dune.ciwebsitebuilder.cli:cli',
            ],
        },
)
