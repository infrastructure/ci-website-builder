#!/bin/bash

set -e

# make sure we are up to date
apt-get update

# Install apt-utils first -> this gives a warning that apt-utils is not set up
apt-get install --yes --no-install-recommends apt-utils

# Now this runs without warnings
DEBIAN_FRONTEND=noninteractive apt-get install --yes \
    hugo \
    git \
    git-lfs \
    doxygen \
    python3-sphinx \
    python3-pip \
    python3-virtualenv \
    python3-dev \
    cmake \
    curl \
    pkg-config \
    pandoc \
    latexmk \
    texlive-full \
    bibtex2html \
    graphviz \
    inkscape \
    rsync \
    g++ \
    gfortran \
    libopenmpi-dev \

# Create user and group that the builder will run under
groupadd builder
useradd -g builder builder

# Create home directory (needed for inkscape)
mkdir /home/builder
chown builder:builder /home/builder

# Create base directory for builds
mkdir /builds
chown builder:builder /builds

# Install linker checker
curl https://htmltest.wjdp.uk | bash -s -- -b /home/builder/bin

# Remove temporary files
apt-get clean
rm -rf /var/lib/apt/lists/*
rm install.sh
